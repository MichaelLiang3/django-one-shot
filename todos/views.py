from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    list_todo_list = TodoList.objects.all()

    context = {
        "list_todo_list": list_todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)

    context = {
        "details": details,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            details = form.save()
            return redirect("todo_list_detail", id=details.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create_todo.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm(instance=todo)
    context = {
        "form": form,
        "todo": todo,
    }
    return render(request, "todos/edit_todo.html", context)


def todo_list_delete(request, id):
    instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete_todo.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoItemForm(instance=task)
    context = {"form": form}
    return render(request, "todos/edit_item.html", context)
